#!/usr/bin/env bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode
set -euo pipefail

usage()
{
  cat <<END
install-dependencies.sh: install dependencies to run mrbot.

  --ssh-token
    Specifies ssh auth token.
  -h | --help
    Displays this help text and exits the script.
  
It is assumed that the microprocessor is raspberry pi.
END
}

arch=$(arch)
ssh_auth_token=''
while [[ $# -gt 0 ]]; do
  case "$1" in
    -h | --help )
      usage; exit 1 ;;
    --ssh-token )
      ssh_auth_token="$2"; shift 2;;
    *)
      echo "Unknown option $1"
      usage; exit 2 ;;
  esac
done

if [[ -z $ssh_auth_token ]]; then
  echo "No SSH authtoken specified. Setup is cancelled."
  usage
  exit 1
fi

echo "Installing Dependencies"

if [ -x "$(command -v docker)" ]; then
    echo "Docker already Installed"
else
    echo "Installing Docker"
    sudo apt update -y
    sudo apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88

    if [ $arch = 'aarch64' ]; then
    sudo add-apt-repository \
        "deb [arch=arm64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
    elif [$arch = 'x86_64' ]; then
    sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
    else
      echo 'this arch is not supported.'
      exit;
    fi
    
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y
    sudo usermod -aG docker mrbot
    
    echo "Docker Installed"
fi

if [ -x "$(command -v ngrok)" ]; then
    echo "NGROK already Installed"
else
    echo "Installing NGROK"
    if [ $arch = 'aarch64' ]; then
      sudo cp ./dependencies/arm64/ngrok /usr/local/bin
    elif [$arch = 'x86_64' ]; then  
      sudo cp ./dependencies/amd64/ngrok /usr/local/bin
    else
      echo 'this arch is not supported.'
      exit;
    fi
    sudo chmod 777 /usr/local/bin/ngrok
    ngrok authtoken $ssh_auth_token
    echo "NGROK Installed"
fi

echo "Dependencies Installed"