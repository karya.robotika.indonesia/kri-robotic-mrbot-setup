#!/usr/bin/env bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode
set -euo pipefail

usage()
{
  cat <<END
install-dependencies.sh: install dependencies to run mrbot.

  --ssh-token
    Specifies ssh auth token.
  -h | --help
    Displays this help text and exits the script.
  
It is assumed that the microprocessor is raspberry pi.
END
}

arch=$(arch)
ssh_auth_token=''
while [[ $# -gt 0 ]]; do
  case "$1" in
    -h | --help )
      usage; exit 1 ;;
    --ssh-token )
      ssh_auth_token="$2"; shift 2;;
    *)
      echo "Unknown option $1"
      usage; exit 2 ;;
  esac
done

if [[ -z $ssh_auth_token ]]; then
  echo "No SSH authtoken specified. Setup is cancelled."
  usage
  exit 1
fi

echo "Installing Dependencies"

if [ -x "$(command -v dotnet)" ]; then
    echo "Dotnet already Installed"
else
    echo "Installing Dotnet"
    
    if [ $arch = 'aarch64' ]; then
      sudo apt-get update \
      && sudo apt-get install -y --no-install-recommends \
          ca-certificates \
          libc6 \
          libgcc1 \
          libgssapi-krb5-2 \
          libicu63 \
          libssl1.1 \
          libstdc++6 \
          zlib1g \
      && sudo rm -rf /var/lib/apt/lists/*
      dotnet_version=3.1.8 \
        && curl -SL --output dotnet.tar.gz https://dotnetcli.azureedge.net/dotnet/Runtime/$dotnet_version/dotnet-runtime-$dotnet_version-linux-arm64.tar.gz \
        && dotnet_sha512='6e2f0f8f30391f33d2862966424dad84b21d039fddc165299d2fb98f1054361bc28bf8199d3737dc24c443181fd512dd5f7738405506ac7999e9141748ba0f20' \
        && echo "$dotnet_sha512 dotnet.tar.gz" | sha512sum -c - \
        && sudo mkdir -p /usr/share/dotnet \
        && sudo tar -ozxf dotnet.tar.gz -C /usr/share/dotnet \
        && rm dotnet.tar.gz

      dotnet_sdk_version=3.1.402 \
        && curl -SL --output dotnet.tar.gz https://dotnetcli.azureedge.net/dotnet/Sdk/$dotnet_sdk_version/dotnet-sdk-$dotnet_sdk_version-linux-arm64.tar.gz \
        && dotnet_sha512='1704fb88a04cb5a00de5a22da4ad68237a7cce9aeaac608166f85082a4bfc86ba360b1d81c5ceb40c906cf7407e365aed73fa68a735d4ba90b334ab01a3a8455' \
        && echo "$dotnet_sha512 dotnet.tar.gz" | sha512sum -c - \
        && sudo tar -ozxf dotnet.tar.gz -C /usr/share/dotnet \
        && rm dotnet.tar.gz \
        && sudo ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet
    elif [ $arch = 'x86_64' ]; then  
      curl -SL --output dotnet.tar.gz https://download.visualstudio.microsoft.com/download/pr/f01e3d97-c1c3-4635-bc77-0c893be36820/6ec6acabc22468c6cc68b61625b14a7d/dotnet-sdk-3.1.402-linux-x64.tar.gz \
        && sudo mkdir -p /usr/share/dotnet \
        && sudo tar -ozxf dotnet.tar.gz -C /usr/share/dotnet \
        && rm dotnet.tar.gz

      dotnet_sdk_version=3.1.402 \
        && curl -SL --output dotnet.tar.gz https://download.visualstudio.microsoft.com/download/pr/f7c8f82a-8c47-497d-875b-2ac210599ec5/e8aea0c195efed8a9aff2ba687db8c26/aspnetcore-runtime-3.1.8-linux-x64.tar.gz \
        && sudo tar -ozxf dotnet.tar.gz -C /usr/share/dotnet \
        && rm dotnet.tar.gz \
        && sudo ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet
    else
      echo 'this arch is not supported.'
      exit;
    fi
    echo "Dotnet Installed"
fi

if [ -x "$(command -v docker)" ]; then
    echo "Docker already Installed"
else
    echo "Installing Docker"
    sudo apt update -y
    curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh
    sudo usermod -aG docker pi
    echo "Docker Installed"
fi

if [ -x "$(command -v docker-compose)" ]; then
    echo "docker-compose already Installed"
else
    echo "Installing docker-compose"

    if [  $arch = 'aarch64' ]; then
      sudo apt-get install -y libffi-dev \
        libssl-dev \
        python3 \
        python3-pip
        
      sudo apt-get remove python-configparser
      sudo pip3 -v install docker-compose
    else
      echo 'this arch is not supported.'
      exit;
    fi

    echo "docker-compose Installed"
fi

if [ -x "$(command -v ngrok)" ]; then
    echo "NGROK already Installed"
else
    echo "Installing NGROK"
    if [ $arch = 'aarch64' ]; then
      sudo cp ./dependencies/arm64/ngrok /usr/local/bin
    elif [ $arch = 'x86_64' ]; then  
      sudo cp ./dependencies/amd64/ngrok /usr/local/bin
    else
      echo 'this arch is not supported.'
      exit;
    fi
    sudo chmod 777 /usr/local/bin/ngrok
    ngrok authtoken $ssh_auth_token
    echo "NGROK Installed"
fi
echo "Dependencies Installed"