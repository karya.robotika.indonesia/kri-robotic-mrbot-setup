v4l2-ctl --overlay=1
echo "Press any key to continue"
user_input=""
while [ -z "$user_input" ] ; do
  read -s -p "waiting for the keypress..." user_input

  if [ -z "$user_input" ] ; then
    v4l2-ctl --overlay=0
    echo "Setting up firmware..."
    
    docker rm collector -f
    docker run -e RobotDelay=10000 -e MaterialRecoveryUrl=https://dev-api.neutrios.com/material-recovery-api  --privileged --restart unless-stopped --name collector -it karyarobotikaindonesia/materialrecoverybot.datacollector:latest
    
    echo "Firmware configured."
    exit ;
  fi
done