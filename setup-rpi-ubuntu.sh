#!/usr/bin/env bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode
set -euo pipefail

usage()
{
  cat <<END
update-rpi-arm64.sh: update raspberry pi from arm32 to arm64.
  -h | --help
    Displays this help text and exits the script.
  
It is assumed that the microprocessor is raspberry pi.
END
}

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h | --help )
      usage; exit 1 ;;
    *)
      echo "Unknown option $1"
      usage; exit 2 ;;
  esac
done

cp ./boot/firmware /boot/firmware
echo "You are good... Rebooting..."
#sudo reboot
