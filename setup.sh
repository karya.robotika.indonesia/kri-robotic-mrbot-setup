#!/usr/bin/env bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode
set -euo pipefail

reconfigureENV()
{
  echo "Configuring $1"
  sudo sh -c "echo export $1="$2" >> /etc/profile"
    # sudo sed -i "/export $1/c\export $1=$3" /etc/profile
  echo "$1 has been configured to $2"
}

usage()
{
  cat <<END
setup.sh: setup the Material Recovery Robot firmware.
Parameters:
  --robot-uuid
    Robot Unique Universal ID.
  --robot-secret
    Robot Secret.
  --robot-scope
    Robot Scope. (default: materialrecovery)
  --robot-delay
    Robot Delay. (default: 3000)
  --auto-update-delay
    Auto Update Delay. (default: 30000)
  --backgroundservice-imagename
    Background Service Image Name. (default: materialrecoverybot.backgroundservices)
  --frontend-imagename
    Frontend Service Image Name. (default: materialrecoverybot.desktop)
  -r | --registry <docker registry>
    Specifies the container registry to use (default: karyarobotikaindonesia).
  --env
    Specifies Environment. Available Env : dev, prod, local (default: dev).
  
  --materialrecovery-dns <dns or ip address>
    Specifies the materialrecovery DNS/ IP address of the Kubernetes cluster.
  --materialrecoverysignalr-dns <dns or ip address>
    Specifies the materialrecoverysignalr DNS/ IP address of the Kubernetes cluster.
  --identity-dns <dns or ip address>
    Specifies the identity DNS/ IP address of the Kubernetes cluster.

  --dev-materialrecovery-dns <dns or ip address>
    Specifies the dev-materialrecovery DNS/ IP address of the Kubernetes cluster.
  --dev-materialrecoverysignalr-dns <dns or ip address>
    Specifies the dev-materialrecoverysignalr DNS/ IP address of the Kubernetes cluster.
  --dev-identity-dns <dns or ip address>
    Specifies the dev-identity DNS/ IP address of the Kubernetes cluster.
    
  --local-materialrecovery-dns <dns or ip address>
    Specifies the local-materialrecovery DNS/ IP address of the Kubernetes cluster.
  --local-materialrecoverysignalr-dns <dns or ip address>
    Specifies the local-materialrecoverysignalr DNS/ IP address of the Kubernetes cluster.
  --local-identity-dns <dns or ip address>
    Specifies the local-identity DNS/ IP address of the Kubernetes cluster.
    
  --ssh-token
    Specifies ssh auth token.
  -h | --help
    Displays this help text and exits the script.
  
It is assumed that the robot firmware isn't installed yet.
WARNING! THE SCRIPT WILL COMPLETELY DESTROY ALL DEPLOYMENTS AND SERVICES VISIBLE
FROM THE CURRENT CONFIGURATION CONTEXT AND NAMESPACE.
It is recommended that you check your selected namespace, 'eshop' by default, is already in use.
Every deployment and service done in the namespace will be deleted.
For more information see https://kubernetes.io/docs/tasks/administer-cluster/namespaces/
END
}
robot_uuid=''
robot_secret=''
robot_type='mrbot'
robot_scope='materialrecovery'
robot_delay='3000'
auto_update_delay='30000'
materialrecovery_dns='https://api.neutrios.com/material-recovery-api'
materialrecoverysignalr_dns='https://api.neutrios.com/material-recovery-signalrhub'
identity_dns='https://identity.neutrios.com'
dev_materialrecovery_dns='https://dev-api.neutrios.com/material-recovery-api'
dev_materialrecoverysignalr_dns='https://dev-api.neutrios.com/material-recovery-signalrhub'
dev_identity_dns='https://dev-identity.neutrios.com'
local_materialrecovery_dns='http://localhost:5601'
local_materialrecoverysignalr_dns='http://localhost:5602'
local_identity_dns='http://localhost:5001'
registry='karyarobotikaindonesia'
is_local_build=false
environment='Development'
backgroundservice_imagename='materialrecoverybot.backgroundservices'
frontend_imagename='materialrecoverybot.desktop'
ssh_auth_token=''

while [[ $# -gt 0 ]]; do
  case "$1" in
    --materialrecovery-dns )
      materialrecovery_dns="$2";shift 2;;
    --materialrecoverysignalr-dns )
      materialrecoverysignalr_dns="$2";shift 2;;
    --identity-dns )
      identity_dns="$2";shift 2;;
    --dev-materialrecovery-dns )
      dev_materialrecovery_dns="$2";shift 2;;
    --dev-materialrecoverysignalr-dns )
      dev_materialrecoverysignalr_dns="$2";shift 2;;
    --dev-identity-dns )
      dev_identity_dns="$2";shift 2;;
    --local-materialrecovery-dns )
      local_materialrecovery_dns="$2";shift 2;;
    --local-materialrecoverysignalr-dns )
      local_materialrecoverysignalr_dns="$2";shift 2;;
    --local-identity-dns )
      local_identity_dns="$2";shift 2;;
    --env )
      if [ $2 == 'dev' ]; then 
        environment='Development'
        is_local_build=false
      elif [ $2 == 'local' ]; then
        environment='Development'
        is_local_build=true
      elif [ $2 == 'prod' ]; then
        environment='Production'
        is_local_build=false
      fi;shift 2;;
    -h | --help )
      usage; exit 1 ;;
    -r | --registry )
      registry="$2"; shift 2;;
    --skip-clean )
      clean=''; shift ;;
    --robot-uuid )
      robot_uuid="$2"; shift 2;;  
    --robot-secret )
      robot_secret="$2"; shift 2;;  
    --robot-scope )
      robot_scope="$2"; shift 2;;
    --robot-delay )
      robot_delay="$2"; shift 2;;
    --auto-update-delay )
      auto_update_delay="$2"; shift 2;;
    --backgroundservice-imagename )
      backgroundservice_imagename="$2"; shift 2;;
    --frontend-imagename )
      frontend_imagename="$2"; shift 2;;
    --ssh-token )
      ssh_auth_token="$2"; shift 2;;
    *)
      echo "Unknown option $1"
      usage; exit 2 ;;
  esac
done

bash ./install-dependencies.sh --ssh-token $ssh_auth_token

# Initialization & check commands
if [[ -z $robot_uuid ]]; then
  echo "No UUID specified. Setup is cancelled."
  usage
  exit 1
fi

if [[ -z $robot_secret ]]; then
  echo "No Secret specified. Setup is cancelled."
  usage
  exit 1
fi

echo "Setting up env vars..."
echo "Reset profile config..."

arch=$(arch)
if [ $arch = 'aarch64' ]; then  
  sudo cp $HOME/kri-robotic-mrbot-setup/etc/profile /etc
elif [ $arch = 'x86_64' ]; then
  sudo cp $HOME/kri-robotic-mrbot-setup/etc/profile.ubuntu /etc/profile
else
    echo 'this arch is not supported.'
    exit;
fi

reconfigureENV BACKGROUNDSERVICE_IMAGENAME "$registry/$backgroundservice_imagename"
reconfigureENV FRONTEND_IMAGENAME "$registry/$frontend_imagename"
reconfigureENV ROBOT_DELAY $robot_delay
reconfigureENV ROBOT_SCOPE $robot_scope
reconfigureENV ROBOT_SECRET $robot_secret
reconfigureENV ROBOT_TYPE $robot_type
reconfigureENV IDENTITY_URL $identity_dns
reconfigureENV MATERIALRECOVERY_URL $materialrecovery_dns
reconfigureENV MATERIALRECOVERYSIGNALR_URL $materialrecoverysignalr_dns
reconfigureENV DEV_IDENTITY_URL $dev_identity_dns
reconfigureENV DEV_MATERIALRECOVERY_URL $dev_materialrecovery_dns
reconfigureENV DEV_MATERIALRECOVERYSIGNALR_URL $dev_materialrecoverysignalr_dns
reconfigureENV LOCAL_IDENTITY_URL $local_identity_dns
reconfigureENV LOCAL_MATERIALRECOVERY_URL $local_materialrecovery_dns
reconfigureENV LOCAL_MATERIALRECOVERYSIGNALR_URL $local_materialrecoverysignalr_dns
reconfigureENV AUTO_UPDATE_DELAY $auto_update_delay
reconfigureENV ROBOT_DELAY $robot_delay
reconfigureENV ROBOT_UUID $robot_uuid
reconfigureENV IS_LOCAL_BUILD $is_local_build
reconfigureENV ENVIRONMENT $environment
echo "Env vars setted up ..."

echo "Checking is public key exists?"
ssh_public_file="$HOME/.ssh/id_ed25519"
if [ -f "$ssh_public_file" ]; then
  echo "$ssh_public_file exists."
else
  echo "Generating ssh public key"
  ssh-keygen -t ed25519 -C "$robot_uuid"
fi

ssh_public_key=$(cat $HOME/.ssh/id_ed25519.pub)
echo $ssh_public_key
echo "Press any key to continue after ssh registered"
user_input=""
while [ -z "$user_input" ] ; do
  read -s -p "waiting for the keypress..." user_input

  if [ -z "$user_input" ] ; then
    if [ -d "$HOME/kri-robotic-mrbot-deployment" ]; then
      echo "MRBot Deployment source code existed"
      cd $HOME/kri-robotic-mrbot-deployment
      git pull origin master
      echo "MRBot Deployment source code updated"
    else
      cd $HOME
      deployment_url='git@gitlab.com:karya.robotika.indonesia/kri-robotic-mrbot-deployment.git'
      echo "Pulling MRBot Deployment source code"
      git clone $deployment_url
      echo "MRBot Deployment source code pulled"
    fi
    
    if [ -d "$HOME/kri-robotic-deployment" ]; then
      echo "Deployment source code existed"
      cd $HOME/kri-robotic-deployment
      git pull origin master
      echo "Deployment source code updated"
    else
      cd $HOME
      deployment_url='git@gitlab.com:karya.robotika.indonesia/kri-robotic-deployment.git'
      echo "Pulling Deployment source code"
      git clone $deployment_url
      echo "Deployment source code pulled"
    fi

    echo "Setting up firmware..."

    bash $HOME/kri-robotic-mrbot-deployment/install.sh
    
    echo "Firmware configured."
    echo "Rebooting"
    exit ;
  fi
done
# reboot
